**Was bedeutet NIFTI und FNIT?**

Die Abkürzung FNIT steht für Frauen, nichtbinäre, intersex und transgender Personen.
Menschen im FNIT-Spektrum sind im technischen Bereich stark unterrepräsentiert. Dadurch wird  Technik entwickelt, die die Bedürfnisse der FNIT Menschen nicht berücksichtigt oder sie sogar unterdrückt. Außerdem befinden sich im technischen Bereich stabile, gute bezahlte Arbeitsfelder. Es gibt keinen Grund, diese Situation als gegeben hinzunehmen.

**Warum sollte mensch eine lokale Gruppe aufbauen wollen?**

Wenn du auf dieser Seite bist, bist du vermutlich Frau, nichtbinär, inter oder trans. Wann hast du das letzte Mal mit einer anderen Person im FNIT-Spektrum über Technik geredet? Eben. Es gibt sie aber und durch die Gründung einer solchen Gruppe kannst du sie in deinem Ort finden. Wir haben durch unsere Gruppen festgestellt, wie viel Spaß und neue Ideen dabei entstehen und wünschen uns, dass jede Person diese Erfahrung machen kann und wir die Gesellschaft dabei gleichzeitig weiter entwickeln.

**Wie kannst du eine lokale Gruppe aufbauen?**

Eine lokale Gruppe benötigt einen Ort, eine Uhrzeit und mindestens eine Person, die entscheidet, ein Treffen zu veranstalten. Das Thema ist dabei optional.

**Wie findest du einen Ort?**

Jeder Ort mit Sitzgelegenheiten und etwas Internet ist ein guter Ort. Darunter fallen

   * ein ruhiges Lokal
   * dein lokaler Hackspace
   * dein lokaler CCC-Erfa
   * deine Hochschule


**Welche Uhrzeit ist die beste?**

Es gibt viele verschiedene Varianten. Manche treffen sich werktags, häufig Montag bis Donnerstag abends ab 19 Uhr für zwei Stunden. Andere machen Sonntag morgens drei Stunden Treffen. Freitag bis Sonntag Abend sind generell die nicht die besten Abende, aber Ausnahmen gibt es immer.

**Wie erreichst du Menschen?**

Falls du in einem Hackspace dein Treffen machst, frage sie, ob sie das Treffen über ihre Kanäle wie Twitter, Mastodon und Blog publizieren können. Viele Gruppen haben sich dazu einen eigenen Twitter-Kanal geklickt. Wenn du mit diesem eine der oben auf der Karte gezeigten Nachbargruppen antipst, hilft sie dir sicher bei der Verbreitung.

**Welche Themen kannst du besprechen?**

Es gibt Gruppen, die gerne Vorträge anbieten. Andere stimmen zweimal im Jahr über den Schwerpunkt der nächsten Monate ab und arbeiten dann jeweils am gemeinsamen Projekt. Wieder andere unterhalten sich gerne ohne initial vorgegebenes Thema. Du könntest am ersten Treffen mit den anderen diese Seite anschauen, lokale Gruppen nachschlagen und dich dann für die nächsten Monate erstmal auf ein Format festlegen.

**Du bist kein\*e Experte*in?**

Macht nichts, die Reise ist das Ziel. Und zusammen reisen macht bekanntlich mehr Spaß.

**Regelmäßige vs. spontan geplante Treffen?**

Spontan geplante Treffen (weniger als zwei Wochen Vorwarnzeit) treibt Menschen mit Terminkalendern in den Wahnsinn. Lege am Anfang oder der Mitte des jeweiligen Treffen den Termin für das nächste sofort fest.

**Du hast einen Hackspace vor Ort, der dir aber die Rechte für ein Treffen einmal im Monat für drei Stunden verweigert?**

Kontaktiere dafür am Besten die Haecksen unter info@haecksen.org
Sie besitzen speziell konfektionierte Nussknacker zum Lösen solcher Probleme und können dir helfen zu entscheiden, welche Variante in deinem Fall die beste wäre.

**Du bist bereits Teil einer Gruppe und möchtest sie auf der Karte eintragen lassen?**

Du solltest den Code forken und einen merge-request mit deiner Gruppe stellen.
Deine Gruppe wird nicht aufgenommen, wenn sie 

   * trans/ inter/ nicht-binär/ queer - feindlich (zB trans Frauen nicht als Frauen ansieht)
   * rassistisch ist
   * für Überwachung ist.

Wir behalten uns vor, stark von der Industrie finanzierte Gruppen auf ihre inhaltliche Unabhängigkeit zu prüfen.

**Wer seid ihr eigentlich?**

"Wir" sind zum einen die Haecksen, Frauen im Chaos Computer Club (CCC) nebst unseren lokalen Gruppen sowie das Netzwerk der Frauen und Computer Kram (F.U.C.K) Gruppen rund um den feministischen Hackspace Heart of Code in Berlin. Außerdem gibt es dazu diverse freie Gruppen, die sich hier unter dem Begriff NIFTI gesammelt haben.
Vielen gemeinsam ist eine ideologische Nähe zum CCC.

**Wie kann ich euch direkt erreichen?**

Offline: auf den CCC Veranstaltungen quer über das Jahr wie auch auf dem Congress oder auf einem der lokalen Treffen. Reisekosten zum gegenseitigen Besuchen können möglicherweise durch den CCC bezuschusst werden.

Online: Lokale Gruppen kannst du häufig per Twitter oder Mail kontaktieren. Die Haecksen erreichts du unter info@haecksen.org. F.U.C.K als Netzwerk hat keinen zentralen Kontaktpunkt.