# NIFTI map - a derivative of the F.U.C.K map

![screenshot](screenshot.png)

## Add a FNIT space

Add an entry to `static/fucks.json`.

You can have multiple contacts, they are free-form. Just add multiple objects to the array.

There is an empty marker at `static/markers/marker.png` and the GIMP file at `static/markers/marker-all.xcf` that you can use to make your own marker, or reference the standard `static/markers/marker-fuck.png`.

You can figure out the coordinates to your city [with this service](https://www.latlong.net/). Please note that in the JSON they are `[long, lat]`.


``` json
{
  "name": "Name of the space",
  "city": "city of space",
  "coords": [11.58639, 50.92722],
  "img": "static/markers/marker-fuck.png",
  "contacts": [
      {
        "title": "Used in title attribute",
        "url": "https://url.tld",
        "value": "visible link"
      }
  ]
}
```

## Install dependencies

This is a Hugo 0.59 page which needs go 1.11 as a dependency

You can get Hugo through your package manager. Please check its version since
e.g. Ubuntu 18.04 does not have current versions of Hugo and go available.
If necessary get Hugo form the [GitHub release page](https://github.com/gohugoio/hugo/releases) 
as a standalone package and move it to your ~/bin folder. 

**Do not install Hugo via snap.**

## Dev server

``` bash
$ hugo serve
```

## Build site

``` bash
$ hugo
```
The build step outputs a `public/` folder which contains the entire site. This is also what happens during CI.
