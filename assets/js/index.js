window.addEventListener('DOMContentLoaded', function () {
  const tiles = 'mapbox://styles/mapbox/light-v10'
  const token = 'pk.eyJ1IjoibDFzbDFzIiwiYSI6ImNqdThyNTN1YTBjZzAzeXBkYnh1MmxoY2QifQ.YLJY7qUqSRVxWKWKgo4XLQ'

  mapboxgl.accessToken = token

  let fucks = []
  let map = undefined
  let center = [11.8, 51.2]
  let zoom = 5

  map = new mapboxgl.Map({
    container: 'map',
    style: tiles,
    center: center,
    zoom: zoom
  })

  window.fetch('fucks.json')
    .then(function(res) { return res.json() })
    .then(function(data) {
      fucks = fucks.concat(data)
      let markers = fucks.map(markupMarker)

      markers.map(function (obj, i) {
        new mapboxgl.Marker(obj)
          .setLngLat(fucks[i].coords)
          .addTo(map);
      })
    })
    .catch(function(err) {
      console.log('ERROR WILL ROBINSON!', err)
    })

  window.fetch('haecksen.json')
  .then(function(res) { return res.json() })
    .then(function(data) {
      fucks = fucks.concat(data)
      let markers = fucks.map(markupMarker)

      markers.map(function (obj, i) {
        new mapboxgl.Marker(obj)
          .setLngLat(fucks[i].coords)
          .addTo(map);
      })
    })
    .catch(function(err) {
      console.log('ERROR WILL ROBINSON!', err)
    })

  function markupMarker (obj, i) {
    let el = document.createElement('div');
    el.className = 'c__space';

    let el2 = document.createElement('div');
    el2.className = 'c__space__logo';
    let img = document.createElement('img');
    img.src = obj.img;
    el2.appendChild(img);
    el.appendChild(el2);

    let el3 = document.createElement('div');
    el3.className = 'c__space__info';
    let h2 = document.createElement('h2');
    h2.className = 'c__space__name';
    h2.innerHTML = obj.name;
    let p = document.createElement('p');
    p.innerHTML = obj.city;
    let ul = document.createElement('ul');
    ul.classList = 'c__space__contact';

    obj.contacts.forEach(function (o, j) {
      let li = document.createElement('li');
      let a = document.createElement('a');
      a.href = o.url;
      a.title = o.title;
      a.innerHTML = o.value;
      li.appendChild(a);
      ul.appendChild(li);
    })

    el3.appendChild(h2);
    el3.appendChild(p);
    el3.appendChild(ul);
    el.appendChild(el3);

    el.addEventListener('click', function (ev) {
      el.classList.toggle('active');
    })

    return el;
  }
})
